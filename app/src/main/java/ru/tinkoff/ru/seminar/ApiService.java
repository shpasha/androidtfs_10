package ru.tinkoff.ru.seminar;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.tinkoff.ru.seminar.Weather;

public interface ApiService {
    String API_URL = "http://api.openweathermap.org/data/2.5/";
    String APP_ID = "0ee24c1a6cd6a4f011a6d790b01337e9";

    @GET("weather?units=metric&lang=ru&appid=" + APP_ID)
    Observable<Weather> getCurrentWeather(@Query("q") String city);

    @GET("forecast?units=metric&lang=ru&appid=" + APP_ID)
    Observable<List<Weather>> getForecastWeather(@Query("q") String city);
}
